import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

export default function ResultCalculation(props) {
    const {capital, interest, months, total, errorMessage} = props;
    return (
        <View style = {styles.content}>
            {total && (
                <View style = {styles.boxResult}>
                    <Text style={styles.title}>RESUMEN</Text>
                    <DataResult title={"Cantidad solicitdada:"} value={`$ ${capital} `}></DataResult>
                    <DataResult title={"Interés %:"} value={`${interest} %`}></DataResult>
                    <DataResult title={"Plazos %:"} value={`${months} meses`}></DataResult>
                    <DataResult title={"Pago mensual:"} value={`$ ${total.monthlyFee}`}></DataResult>
                    <DataResult title={"Total a pagar:"} value={`$ ${total.totalPayable}`}></DataResult>
                </View>
            )}
            <View>
                <Text style={styles.error}>{errorMessage}</Text>
            </View>
        </View>
    )
}

function DataResult(props) {
    const {title, value} = props;

    return (
        <View style= {styles.value}>
            <Text>{title} </Text>
            <Text>{value}</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    error: {
        textAlign: "center",
        color: "red",
        fontWeight:"bold",
        fontSize: 20
    },
    content: {
        marginHorizontal: 40,
    },
    boxResult: {
        padding: 30,

    },
    title: {
        textAlign: "center",
        fontSize: 20,
        fontWeight: "bold",
        marginBottom: 25
    }, data: {

    },
    value:{
        flexDirection: "row",
        justifyContent: "space-between",
        marginBottom: 20
    }
})
