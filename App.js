import React, { useState, useEffect } from 'react';
import {StyleSheet, View, Text, SafeAreaView, StatusBar, YellowBox, LogBox, Button} from 'react-native';
import Footer from './src/components/Footer';
import Form from './src/components/Form';
import ResultCalculation from './src/components/ResultCalculation';
import colors from './src/utils/colors';

//YellowBox.ignoreWarnings(["Picker has been extracted"]);
LogBox.ignoreLogs(["Picker has been extracted"]);

export default function App() {
  const [capital, setCapital, ] = useState(null);
  const [interest, setInterest, ] = useState(null);
  const [months, setMonths] = useState(null);
  const [total, setTotal] = useState(null);
  const [errorMessage, setErrorMessage] = useState("")

  useEffect(() => {
    if(capital && interest && months) calculate();
    else reset();
    
  }, [capital, interest, months])


  const calculate = () => {
    reset();
    if(!capital) { 
      setErrorMessage('Añade la cantidad que necesitas solicitar');
    } else if(!interest){
      setErrorMessage("Añade el interes");
    } else if(!months){
      setErrorMessage("Selcciona el mes");
    }else{
      const i = interest / 100;
      const fee = capital / ((1-Math.pow(i + 1, -months)) / i);
      setTotal({
        monthlyFee: fee.toFixed(2).replace('.', ','),
        totalPayable : (fee * months).toFixed(2).replace('.', ','),
      })
    }
  } 

  const reset = () => {
    setErrorMessage(""),
    setTotal(null);
  }

  return (
    <>
      <StatusBar barStyle="light-content"></StatusBar>
      <SafeAreaView style={styles.safeArea}>
        <View style = {styles.background}></View>
        <Text style={styles.titleApp}>Cotizador de Préstamos</Text>
        <Form 
          setCapital = {setCapital} 
          setInterest = {setInterest} 
          setMonths = {setMonths}></Form>
      </SafeAreaView>

      <ResultCalculation 
        capital = {capital}
        interest = {interest}
        months = {months}
        total = {total}
        errorMessage = {errorMessage}></ResultCalculation>

      <Footer calculate = {calculate}></Footer>
    </>
  );
}

const styles = StyleSheet.create({
  safeArea: {
    height: 290,
    alignItems: 'center',
  },
  background:{
    backgroundColor: colors.PRIMARY_COLOR,
    borderBottomLeftRadius: 50,
    borderBottomRightRadius: 50,
    width: '100%',
    height: 200,
    position: "absolute",
    zIndex: -1
  },
  titleApp: {
    fontSize: 25,
    fontWeight: "bold",
    color: "#fff",
    marginTop: 15,
  }
});
